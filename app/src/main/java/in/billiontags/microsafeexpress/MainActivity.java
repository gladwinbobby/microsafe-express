package in.billiontags.microsafeexpress;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

public class MainActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    Toolbar mToolbar;
    NavigationView mNavigationView;
    ActionBarDrawerToggle mDrawerToggle;
    SliderLayout mSliderLayout;
    TextView mTextViewTrack, mTextViewCalc, mTextViewLocate, mTextViewPickup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.layout_drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        mTextViewTrack = (TextView) findViewById(R.id.txt_track);
        mTextViewCalc = (TextView) findViewById(R.id.txt_calc);
        mTextViewLocate = (TextView) findViewById(R.id.txt_locate);
        mTextViewPickup = (TextView) findViewById(R.id.txt_pickup);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.app_name, R.string.drawer_close);

        setSupportActionBar(mToolbar);

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mSliderLayout = (SliderLayout) findViewById(R.id.slider);
        mSliderLayout.addSlider(new DefaultSliderView(this).image(R.drawable.slider1));
        mSliderLayout.addSlider(new DefaultSliderView(this).image(R.drawable.slider2));
        mSliderLayout.addSlider(new DefaultSliderView(this).image(R.drawable.slider3));
        mSliderLayout.addSlider(new DefaultSliderView(this).image(R.drawable.slider4));

        mTextViewTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity.launch(MainActivity.this, TrackShipmentActivity.class);
            }
        });

        mTextViewCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity.launch(MainActivity.this, CalculateRateTimeActivity.class);
            }
        });

        mTextViewLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity.launch(MainActivity.this, LocateUsActivity.class);
            }
        });

        mTextViewPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity.launch(MainActivity.this, RequestPickupActivity.class);
            }
        });
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
